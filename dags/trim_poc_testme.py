import datetime as dt

from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from operators.fake_operator import FakeOperator


default_args = {
    'owner': 'airflow',
    'start_date': dt.datetime(2020, 11, 20, 00, 00, 00),
    'concurrency': 1,
    'retries': 0
}

def splitScript():
    # TODO: common SQL path? computed by os/sys? Variable? 
    file_name = '/opt/airflow-jenkins/projects/trimurti_airflow_poc/SQLs/fake.sql'
    with open(file_name) as sql_file:
        sql_list = list(filter(None, sql_file.read().split(';')))

    print(sql_list)
    return sql_list

with DAG('001_jenkins_poc_testamelo', default_args=default_args) as dag:
    ping = BashOperator(task_id='ping', bash_command='echo "PING"')
    pong = FakeOperator(task_id='pong', bash_command='echo "PONG"')
    pang = PythonOperator(task_id='pang', python_callable=splitScript)

ping >> pong >> pang
