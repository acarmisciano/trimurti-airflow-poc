# Airflow CI/CD Proof Of Concept


We want to store projects in separate folders and deploy on server via CI/CD. 
For these reasons we're trying to implement conventions and procedures using:
- custom repository structure
- Jenkins pipeline
- a _dag factory_ code
## How it works

- All projects have separated GIT repository.
- All projects have a custom `Jenkinsfile` that describe the related pipeline.
- All projects are deployed on servers in a specific path: `/opt/airflow-jenkins/projects`
- We've added a _dag factory_ code inside Airflow dags path. This component "inject" module from the custom path in Airflow `globals`. To do this we have two constraints:
    - strict repository structure
    - custom `__init__.py` file in `dags` folder of the repo that tell the _dag factory_ which dags needs to be loaded.

The development workflow is pretty simple, for example:

1. developer commit and push code on `development` branch (to be confirmed)
2. Bitbucket send webhook to Jenkins server
3. Jenkins start the pipeline on the node labeld as _development_
4. The pipeline will **check the code** and *move it* to the right place
5. As a feedback to developer last step of pipeline shows the output of `airflow list_dags` cmd and anyone can check if dags are loaded correctly

## Project structure

Following an example of project structure:

```
.
├── dags
│   ├── __init__.py
│   ├── operators
│   │   ├── fake_operator.py
│   │   ├── __init__.py
│   └── trim_poc_testme.py
├── Jenkinsfile
├── README.md
├── SQLs
│   └── fake.sql
└── tests
    ├── __init__.py
    └── test.py
```

As mentioned before all dags should be putted in a `dag` folder with a custom `__init__.py` that allows dag factory to scan inside `.dags/`:

```
import sys
import os
sys.path.append(os.path.dirname(__file__))

from trim_poc_testme import dag as trim_poc_testme


DAGS = [ trim_poc_testme ]
```

In the `__init__.py` file all dags that we like to be loaded by Airflow must be appended in the `DAGS` array.

## TODO

Something to do...

- [ ] Complete pipeline by implementing agent labeling
- [ ] Add notifications to Jenkins pipeline
- [ ] Check if linter or something better thank compile should be used for validating python
- [ ] Provide useful basic test suite
- [ ] Dynamically populate `DAGS` in `__init__.py` by scanning dags folder 
